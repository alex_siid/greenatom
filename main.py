from fastapi import FastAPI, File, UploadFile, Depends, HTTPException
from fastapi.responses import FileResponse, Response, RedirectResponse, JSONResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
import uvicorn
import os
from typing import List, Optional
from model import Inbox, Request, SessionLocal, Session, datetime, timedelta, User, uuid4
from schemas import Images, Item, BaseModel, UserCreate
import shutil
import io
from base64 import encodebytes
from PIL import Image
from werkzeug.security import generate_password_hash,  check_password_hash


app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def check_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=401,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        user = db.query(User).filter(User.username == username)
        if not user:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    return token


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


@app.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = db.query(User).filter(User.username == form_data.username).first()
    if not user or not check_password_hash(user.password_hash, form_data.password):
        raise HTTPException(status_code=401, detail="Incorrect username or password")
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.post("/sign_up")
async def sign_up(user: UserCreate, db: Session = Depends(get_db)):
    if user.username and user.password:
        if db.query(User).filter(User.username == user.username).first():
            return JSONResponse(content={"message": "User with this username already exist"}, status_code=400)
        password_hash = generate_password_hash(user.password)
        new_user = User(username=user.username, password_hash=password_hash)
        db.add(new_user)
        db.commit()
        return Response(status_code=200)


@app.get("/")
async def get():
    return RedirectResponse("http://127.0.0.1:8000/docs")


def get_response_image(image_path):
    pil_img = Image.open(image_path, mode='r')  # reads the PIL image
    byte_arr = io.BytesIO()
    pil_img.save(byte_arr, format='JPEG')  # convert the PIL image to byte array
    encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii')  # encode as base64
    return encoded_img


def get_query(db, id):
    query = db.query(Inbox).filter(Inbox.request_code == id)
    if not query.all():
        raise HTTPException(404, "Invalid request code")
    return query


@app.get("/frame/get/{id}", response_model=Images)
async def get(id: int, token: str = Depends(check_current_user), db: Session = Depends(get_db)):
    query = get_query(db, id)
    folder = query[0].registration_date.strftime("%Y-%m-%d")
    path = f'.\\data\\{folder}\\'
    images = []
    for obj in query.all():
        image = Item(file_name=str(obj.file_name),
                     registration_date=str(obj.registration_date),
                     image=get_response_image(path + str(obj.file_name)))
        images.append(image)
    return Images(items=images)


@app.put("/frame/")
async def add(files: List[UploadFile] = File(...), token: str = Depends(check_current_user), db: Session = Depends(get_db)):
    date = datetime.utcnow().strftime("%Y-%m-%d")

    if not os.path.exists(f'.\\data\\{date}'):
        os.mkdir(f'.\\data\\{date}')

    db_request = Request()
    db.add(db_request)
    db.commit()
    for file in files[:15]:
        if file.filename.split('.')[-1] != 'jpeg':
            raise HTTPException(422, "Incorrect files extension")
        db_img = Inbox(file_name=str(uuid4())+'.jpeg', request_code=db_request.id)
        db.add(db_img)
        db.commit()
        contents = await file.read()
        with open(os.path.join(f'.\\data\\{date}', str(db_img.file_name)), 'wb') as f:
            f.write(contents)
    return Response(status_code=200)


@app.delete("/frame/delete/{id}")
async def delete(id: int, token: str = Depends(check_current_user), db: Session = Depends(get_db)):
    query = get_query(db, id)
    folder = query[0].registration_date.strftime("%Y-%m-%d")
    path = f'.\\data\\{folder}'

    for obj in query.all():
        if os.path.isfile(f'{path}\\{obj.file_name}'):
            os.remove(f'{path}\\{obj.file_name}')

    if not os.listdir(path):
        os.rmdir(path)
    query.delete()
    db.delete(db.query(Request).get(id))
    db.commit()
    return Response(status_code=200)


if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8000)
