"""empty message

Revision ID: f61f7bcba12d
Revises: 4808fadd5a58
Create Date: 2022-03-20 11:39:26.684341

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f61f7bcba12d'
down_revision = 'a1471f94cbf3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('Inbox', 'file_name',
               existing_type=sa.VARCHAR(),
               nullable=False)
    op.alter_column('User', 'password_hash',
               existing_type=sa.VARCHAR(),
               nullable=False)
    op.alter_column('User', 'username',
               existing_type=sa.VARCHAR(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('User', 'username',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.alter_column('User', 'password_hash',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.alter_column('Inbox', 'file_name',
               existing_type=sa.VARCHAR(),
               nullable=True)
    # ### end Alembic commands ###
