from typing import List, Optional
from datetime import datetime
from pydantic import BaseModel


class Item(BaseModel):
    file_name: str
    registration_date: datetime
    image: bytes


class Images(BaseModel):
    items: List[Item]


class UserCreate(BaseModel):
    username: str
    password: str
