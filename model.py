from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, Session
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey

from datetime import datetime, timedelta
from uuid import uuid4

SQLALCHEMY_DATABASE_URL = "postgresql://GreenAdmin:greenatom@localhost/GreenAtom"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class Request(Base):
    __tablename__ = "Request"

    id = Column(Integer, primary_key=True, autoincrement=True)
    images = relationship("Inbox")


class Inbox(Base):
    __tablename__ = "Inbox"

    id = Column(Integer, primary_key=True, autoincrement=True)
    file_name = Column(String, nullable=False)
    registration_date = Column(DateTime, default=datetime.now())
    request_code = Column(Integer, ForeignKey('Request.id'))


class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String, nullable=False)
    password_hash = Column(String, nullable=False)
