import os
import shutil
import unittest
from main import *
from model import engine, Session, Request, Inbox, datetime, User
from fastapi.testclient import TestClient


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.client = TestClient(app)
        self.fake_user = {
            'username': 'Mr.Anderson',
            'password': 'red_pill',
        }

    def test_sign_up(self):
        db = Session(bind=engine)
        user = db.query(User).filter(User.username == self.fake_user['username']).first()
        response = self.client.post("/sign_up", json=self.fake_user)
        if user:
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.json()['message'], "User with this username already exist")
        else:
            self.assertEqual(response.status_code, 200)

    def test_sign_in(self):
        self.client.post("/sign_up", json=self.fake_user)
        positive_response = self.client.post("/token", data=self.fake_user)
        self.assertEqual(positive_response.json()["token_type"], "bearer")

        negative_response = self.client.post("/token", data={'username': 'aaa', 'password': 'bbb'})
        self.assertEqual(negative_response.status_code, 401)

    def test_get(self):
        image = get_response_image(".\\image.jpeg")
        self.client.post("/sign_up", json=self.fake_user)
        token = self.client.post("/token", data=self.fake_user).json()['access_token']
        db = Session(bind=engine)
        request_id, file = MyTestCase.save_file_fiction(db)
        db_img = db.query(Inbox).filter(Inbox.file_name == file).first()
        data = {"items": [{"file_name": str(db_img.file_name),
                           "registration_date": db_img.registration_date.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                           "image": str(image)}]}

        response = self.client.get("/frame/get/" + str(request_id),  headers={"Authorization": f"Bearer {token}"})
        db.close()
        self.client.delete("/frame/delete/" + str(request_id), headers={"Authorization": f"Bearer {token}"})
        self.assertEqual(response.json(), data)

        negative_response = self.client.get("/frame/get/" + str(-1),  headers={"Authorization": f"Bearer {token}"})
        self.assertEqual(negative_response.status_code, 404)

    def test_delete(self):
        db = Session(bind=engine)
        old_len = len(db.query(Inbox).all())
        self.client.post("/sign_up", json=self.fake_user)
        token = self.client.post("/token", data=self.fake_user).json()['access_token']
        request_id, file = MyTestCase.save_file_fiction(db)
        response = self.client.delete("/frame/delete/" + str(request_id), headers={"Authorization": f"Bearer {token}"})
        new_len = len(db.query(Inbox).all())
        db.close()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(old_len, new_len)

        negative_response = self.client.get("/frame/get/" + str(-1), headers={"Authorization": f"Bearer {token}"})
        self.assertEqual(negative_response.status_code, 404)

    @staticmethod
    def save_file_fiction(db):
        date = datetime.utcnow().strftime("%Y-%m-%d")

        folder = f'.\\data\\{date}\\'

        if not os.path.exists(folder):
            os.mkdir(folder)

        db_request = Request()
        db.add(db_request)
        db.commit()

        db_img = Inbox(file_name=str(uuid4())+'.jpeg', request_code=db_request.id)
        db.add(db_img)
        db.commit()

        shutil.copyfile('.\\image.jpeg', folder + str(db_img.file_name))
        return db_request.id, db_img.file_name


if __name__ == '__main__':
    unittest.main()
